<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorDetail extends Model
{
    use HasFactory;

    protected $table = "vendor_details";

    protected $fillable = [
        'vendor_id',
        'type_id',
        'product_type_id'
    ];

    public function vendor(){
        return  $this->belongsTo(Vendor::class,'id','vendor_id');
    }

    public function vtype(){
        return $this->hasMany(VendorType::class,'id','type_id')->select('id','name')->distinct('name');
    }

    public function ptype(){
        return  $this->hasMany(ProductType::class,'id','product_type_id')->select('id','name');
    }
}
