<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorType extends Model
{
    use HasFactory;

    protected $table = "vendor_types";

    protected $fillable = ['name'];

    public function vdetails(){
        return  $this->belongsTo(VendorDetail::class,'type_id','id');
    }
}
