<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use HasFactory;

    protected $table = "vendors";

    protected $fillable = [
        'name',
        'group_id',
        'phone_number',
        'email',
        'address',
        'origin'
    ];

    public function vgroup(){
        return  $this->hasOne(VendorGroup::class,'id','group_id');
    }

    public function vendor_details(){
        return  $this->hasMany(VendorDetail::class,'vendor_id','id')->with(['vtype','ptype']);
    }


}
