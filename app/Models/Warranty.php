<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{
    use HasFactory;
    protected $table = 'warranties';
    protected $fillable = ['invoice_id','product_name','product_serial_no','description','receiving_date','delivery_date','warranty_type','status'];
}
