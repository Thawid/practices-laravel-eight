<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    use HasFactory;

    protected $table = "employees";

    protected $fillable = ['employee_name','designation','department','email','phone'];

    /*------Mutators--------*/

    public function setEmailAttribute($value){
        $this->attributes['email'] = strtolower($value);
    }


    /*------Accessor-------*/

    public function getEmployeenameAttribute($value){
        return strtoupper($value);
    }

    public static function getEmployee(){
        $allData = DB::table('employees')
            ->select('id','employee_name','designation','department','email','phone')
            ->get()
            ->toArray();
        return $allData;
    }
}
