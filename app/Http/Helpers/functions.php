<?php

function splitName($name){
    $nameArray = trim($name);
    $explode_name = explode(" ", $nameArray);
    $first_name = $explode_name[0];
    $last_name = $explode_name[1];
    return array($first_name,$last_name);
}
