<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    public function uploadForm(){
        return view('admin.fileUpload.uploadForm');
    }

    public function uploadFile(Request $request){
        $this->validate($request,[
            'upload_file' => 'required|image',
        ]);

        $path=$request->file('upload_file')->store('public/upload');
        if($path){
            return back()->with('message','Successfully Upload File');
        }
    }
}
