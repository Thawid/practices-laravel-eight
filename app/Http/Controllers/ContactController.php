<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function contactUs(){
        return view('admin.contact.index');
    }

    public function sendMail(Request $request){

        $details = [
          'name'=>$request->name,
          'email'=>$request->email,
          'phone'=>$request->phone,
          'description'=>$request->description,
        ];

        Mail::to('tanvir@voicelync.com')->send(new ContactMail($details));

        return back()->with('message','Mail Send Successfully');

    }
}
