<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Warranty;
use Illuminate\Support\Facades\Validator;

class WarrantyController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    public function addWarrantyProduct(){
        return view('admin.warranty.addWarrantyProduct');
    }

    public function saveWarranty(Request $request){

        $validateData = $request->validate([
            'invoice_id' => 'required|unique:warranties',
            'product_name' => 'required',
            'receiving_date' => 'required|date',
            'delivery_date' => 'required|date',
            'warranty_type' => 'required',
            'status' => 'required',
            'description' => 'required'
        ]);

        $save_data = DB::table('warranties')->insert(
            [
                'invoice_id' => $request->invoice_id,
                'product_name' => $request->product_name,
                'product_serial_no' =>$request->product_serial_no,
                'receiving_date' =>$request->receiving_date,
                'delivery_date' => $request->delivery_date,
                'warranty_type' => $request->warranty_type,
                'status' => $request->status,
                'description' => $request->description
            ]);

        if($save_data){
           return back()->with('message','New warranty product add successfully');
        }

    }

    public function getAllWarrantyProduct(){
        $allWarrantyProduct = DB::table('warranties')->orderBy('id','DESC')->get();
        return view('admin.warranty.allWarrantyProduct',[
            'allWarrantyProduct' => $allWarrantyProduct
        ]);
    }

    public function updateWarranty($id){
        $data = DB::table('warranties')->where('id',$id)->first();
        return view('admin.warranty.updateWarranty',[
            'update' =>$data
        ]);
    }

    public function updateWarrantyData(Request $request){

        $validator = Validator::make($request->all(), [
            'invoice_id' => 'required',
            'product_name' => 'required',
            'receiving_date' => 'required|date',
            'delivery_date' => 'required|date',
            'warranty_type' => 'required',
            'status' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error','Something went wrong');
        }
       $data =  DB::table('warranties')
            ->where('id',$request->input('id'))
            ->update([
                'invoice_id' => $request->invoice_id,
                'product_name' => $request->product_name,
                'product_serial_no' =>$request->product_serial_no,
                'receiving_date' =>$request->receiving_date,
                'delivery_date' => $request->delivery_date,
                'warranty_type' => $request->warranty_type,
                'status' => $request->status,
                'description' => $request->description
            ]);

        //$warranty->update($request->all());
        if($data){
             return redirect()->route('all.warranty.product')->with('status','Successfully Update');
        }
    }

    public function deleteWarranty($id){
        $delete = DB::table('warranties')->where('id',$id)->delete();
        if($delete) {
            return redirect()->route('all.warranty.product')->with('status','Successfully Delete');
        }else{
            return redirect()->route('all.warranty.product')->with('error','Something went wrong');
        }
    }

    public function warrantyDetails($id){
        $warranty = DB::table('warranties')->where('id',$id)->first();
        return view('admin.warranty.warrantyDetail',[
            'warranty'=>$warranty
        ]);
    }
}
