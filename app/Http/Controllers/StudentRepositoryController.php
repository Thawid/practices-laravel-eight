<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Repositories\StudentInterface;

class StudentRepositoryController extends Controller
{

    protected $student;
    public function __construct(StudentInterface $student){
        $this->student = $student;
    }

    public function index(){
        $students = $this->student->all();
        return view('admin.repository.index',['students'=>$students]);
    }

    public function saveStudent(Request $request){

        /*$student = new Student();
        $student->first_name = $request->first_name;
        $student->last_name = $request->last_name;
        $student->email = $request->email;
        $student->phone = $request->phone;
        $student->save();*/
        $student = $this->student->store($request->all());
        return  response()->json($student);
    }

    public function getStudentById($id){
        $student = $this->student->get($id);
        return response()->json($student);
    }

    public function updateStudent(Request $request){
        /*$student = Student::find($request->id);
        $student->first_name = $request->first_name;
        $student->last_name = $request->last_name;
        $student->email = $request->email;
        $student->phone = $request->phone;
        $student->save();*/
        $student = $this->student->update($request->id,$request->all());
        return  response()->json($student);
    }

    public function deleteStudent($id){
        /*$student = Student::find($id);
        $student->delete();*/
        $this->student->delete($id);
        //return back()->with('delete-success','Record are deleted successfully');
        return response()->json(['success'=>'Record has been deleted']);
    }


    public function deleteCheckedStudent(Request $request){

        $ids = $request->ids;
        Student::whereIn('id',$ids)->delete();
        return response()->json(['success'=>'Student has been delete successfully']);
    }
}
