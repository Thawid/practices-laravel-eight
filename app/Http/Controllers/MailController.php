<?php

namespace App\Http\Controllers;

use App\Mail\TestMil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendMail(){
        $details = [
            'title'=> 'Mail From WinnerDevs',
            'body'=>'This is test mail by using Gmail SMTP'
        ];

        Mail::to('tanvir@voicelync.com')->send(new TestMil($details));
        return back()->with('mail-send','Mail send successfully');
    }
}
