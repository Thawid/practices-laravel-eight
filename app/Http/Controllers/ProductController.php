<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function addProduct(Request $request){

        $product = [
            ['name'=>'Phone'],
            ['name'=>'Laptop'],
            ['name'=>'Desktop'],
            ['name'=>'Television'],
            ['name'=>'iPhone'],
            ['name'=>'Watch'],
            ['name'=>'Water Botol'],
            ['name'=>'Lamp'],
        ];

        Product::insert($product);
        return 'Product has been successfully inserted';
    }

    public function productList(){
        return view('admin.autocomplete.index');
    }
    public function getProduct(Request $request){
        $names = Product::select('name')
            ->where('name','LIKE',"%{$request->name}%")
            ->get();
        return response()->json($names);

    }
}
