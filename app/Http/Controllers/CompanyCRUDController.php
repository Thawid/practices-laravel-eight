<?php

namespace App\Http\Controllers;
use App\Models\Company;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;


class CompanyCRUDController extends Controller
{
    public function index(){
        if(request()->ajax()){
            return datatables()->of(Company::select('*'))
                ->addColumn('action','companies.action')
                ->rawColoum(['action'])
                ->addIndexColoum()
                ->make(true);
        }
        return view('admin.Companies.index');
    }


    public function create(){
        return view('admin.Companies.create');
    }

    public function store(Request $request){

        $request->validate([
            'name'=> 'required',
            'email'=>'required',
            'address'=>'required'
        ]);

        $company = new Company;
        $company->name = $request->name;
        $company->email = $request->email;
        $company->address = $request->address;
        $company->save();
        return redirect()->route('companies.index')->with('success','Company has been created successfully');
    }
}


