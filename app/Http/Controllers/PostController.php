<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(Request $request){

        $posts = Post::paginate(10);
        if($request->ajax()){
            $view = view('admin.post.data',compact('posts'))->render();
            return response()->json(['html'=>$view]);
        }
        return view ('admin.post.index',compact('posts'));
    }


    public function autoload(Request $request){
        $results = Post::paginate(15);
        $artilces = '';
        if ($request->ajax()) {
            foreach ($results as $result) {
                $artilces.='<div class="card mb-2"> <div class="card-body"><h5 class="card-title">'.$result->title.'</h5> '.$result->body.'</div></div>';
            }
            return $artilces;
        }
        return view('admin.post.autoload');
    }
}
