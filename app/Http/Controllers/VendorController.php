<?php

namespace App\Http\Controllers;

use App\Models\Vendor;
use App\Models\VendorDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VendorController extends Controller
{

    public function index(){

        /*$vendor_details = DB::table('vendor_details')
            ->join('vendors','vendors.id','=','vendor_details.vendor_id')
            ->join('vendor_groups','vendor_groups.id','=','vendors.group_id')
            ->join('vendor_types','vendor_types.id','=','vendor_details.type_id')
            ->select('vendors.name as vendor_name','vendor_groups.name as group_name','vendor_types.name as vtype_name')
            ->groupBy('vendor_details.vendor_id')
            ->get();
        dd($vendor_details);*/
        //$vendor_details = VendorDetail::with('vendor','vtype')->get();
        $vendor_details = Vendor::with(['vendor_details','vgroup'])->get();
        //dd($vendor_details);
        /*foreach ($vendor_details as $detail){
            foreach ($detail->vendor_details as $vdetails){
                foreach ($vdetails->vtype as $vtype){
                    dd($vtype);
                }
            }
        }*/
        return view('admin.vendor.index',compact('vendor_details'));
    }
}
