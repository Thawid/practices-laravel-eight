<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Exports\EmployeeExport;
use Excel;
use PDF;
use App\Imports\EmployeeImport;

class EmployeeController extends Controller
{

   public function index(){
       $employees = Employee::orderBy('id','DESC')->get();
       return view('admin.employee.index',['employees'=>$employees]);
   }
    public function addEmployee(){
        return view('admin.employee.create');
    }

    public function storeEmployee(Request $request){
       //dd($request->all());

       $validateData = $request->validate([
           'employee_name'=> 'required',
           'designation'=>'required',
           'department'=>'required',
           'email'=>'required|email|unique:employees',
           'phone'=>'required|unique:employees',
       ]);

       $image = $request->file('profile_picture');
       $imageName = time().'.'.$image->extension();
       $image->move(public_path('employee-profile-pic'),$imageName);
       $employee = new Employee();
       $employee->employee_name = $request->employee_name;
       $employee->designation = $request->designation;
       $employee->department = $request->department;
       $employee->email = $request->email;
       $employee->phone = $request->phone;
       $employee->profile_picture = $imageName;
       $employee->save();
       return redirect()->route('employee')->with('add-employee','Employee added successfully');

    }

    public function viewEmployee($id){
       $employee = Employee::find($id);
       return response()->json($employee);
    }

    public function employeeDetails($id){
       $employee = Employee::find($id);
       return view('admin.employee.viewDetails',[
           'employee'=>$employee
       ]);
    }

    public function editEmployee($id){
       $employee = Employee::find($id);
       return view('admin.employee.edit',['employee'=>$employee]);
    }

    public function updateEmployee(Request $request){
        $employee = Employee::find($request->id);
        if($request->hasFile('profile_picture')):
            $image = $request->file('profile_picture');
            $imageName = time().'.'.$image->extension();
            $image->move(public_path('employee-profile-pic'),$imageName);
            $employee->profile_picture = $imageName;
        endif;
        $employee->employee_name = $request->employee_name;
        $employee->designation = $request->designation;
        $employee->department = $request->department;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->save();
        return redirect()->route('employee')->with('add-employee','Employee update successfully');
    }

    public function deleteEmployee($id){
       $employee = Employee::find($id);
       if(!empty($employee->profile_picture)){
           unlink(public_path('employee-profile-pic'.'/'.$employee->profile_picture));
       }
       $employee->delete();
       return back()->with('delete-message','Employee Delete Successfully');
    }

    public function exportIntoExcel(){
       return Excel::download(new EmployeeExport,'employee-list.xlsx');

    }

    public function exportInCsv(){
       return Excel::download(new EmployeeExport,'employee-list.csv');
    }

    public function pdfView(){
        $employees = Employee::all();
        return view('admin.employee.pdf',compact('employees'));
    }
    public function downloadPdf(){
       $employees = Employee::all();
       $pdf = PDF::loadView('admin.employee.pdf',compact('employees'));
       return $pdf->download('employee-list.pdf');
    }


    public function importEmployeeFromExcelCsv(Request $request){

        Excel::import(new EmployeeImport,$request->file);
        return back()->with('import-file','Record are imported successfully');
    }

}
