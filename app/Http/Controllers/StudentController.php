<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function index(){
        $students = Student::orderBy('id','DESC')->get();
        return view('admin.student.index',['students'=>$students]);
    }

    public function saveStudent(Request $request){

        $student = new Student();
        $student->first_name = $request->first_name;
        $student->last_name = $request->last_name;
        $student->email = $request->email;
        $student->phone = $request->phone;
        $student->save();
        return  response()->json($student);
    }

    public function getStudentById($id){
        $student = Student::find($id);
        return response()->json($student);
    }

    public function updateStudent(Request $request){
        $student = Student::find($request->id);
        $student->first_name = $request->first_name;
        $student->last_name = $request->last_name;
        $student->email = $request->email;
        $student->phone = $request->phone;
        $student->save();
        return  response()->json($student);
    }

    public function deleteStudent($id){
        $student = Student::find($id);
        $student->delete();
        //return back()->with('delete-success','Record are deleted successfully');
        return response()->json(['success'=>'Record has been deleted']);
    }


    public function deleteCheckedStudent(Request $request){

        $ids = $request->ids;
        Student::whereIn('id',$ids)->delete();
        return response()->json(['success'=>'Student has been delete successfully']);
    }

}
