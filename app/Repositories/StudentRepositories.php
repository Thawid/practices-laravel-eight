<?php


namespace App\Repositories;
use App\Models\Student;

class StudentRepositories Implements StudentInterface
{

   public function all(){
       return Student::orderBy('id','DESC')->get();

   }

    public function store(array $data)
    {
        return Student::create($data);
    }




    public function get($id)
    {
        return Student::find($id);
    }


    public function update($id, array $data)
    {
        return  Student::find($id)->update($data);
    }

    public function delete($id)
    {
        return Student::destroy($id);
    }
}
