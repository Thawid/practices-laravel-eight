<?php


namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class PostTableSeeder extends Seeder
{

    public function run(){

        $faker = Faker::create();
        foreach (range(0,100) as $index){
            DB::table('posts')->insert([
                'title'=>$faker->text(20),
                'body'=>$faker->text(300),
                'created_at'=>$faker->dateTimeBetween('-6 month','+1 month')
            ]);
        }
    }
}
