<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,100) as $index){
            DB::table('customers')->insert(
                [
                    'name'=>$faker->name,
                    'email'=>$faker->email,
                    'phone'=>$faker->phoneNumber,
                    'address'=>$faker->address,
                ]
            );
        }
    }
}
