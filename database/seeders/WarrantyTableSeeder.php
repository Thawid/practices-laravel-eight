<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class WarrantyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();
        foreach (range(0,50) as $index){
            DB::table('warranties')->insert([
                'invoice_id' => $faker->numberBetween($min = 6, $max = 8),
                'product_name' => $faker->randomElement(['HP Laptop', 'Dell Laptop','HP Monitor', 'Dell Monitor','Asus Laptop','Asus Monitor']),
                'product_serial_no' => $faker->numberBetween($min = 6, $max = 8),
                'description' => $faker->sentence,
                'receiving_date' => $faker->date(now()),
                'delivery_date'=> $faker->randomElement(['2020-12-10','2020-12-11','2020-12-12','2020-12-13','2020-12-17']),
                'warranty_type'=> $faker->randomElement(['Repair','Replacement']),
                'status'=>$faker->randomElement(['Pending', 'Delivered'])
            ]);
        }

    }
}
