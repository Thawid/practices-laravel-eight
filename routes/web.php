<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\AjaxImageUploadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();


Route::group(['prefix'=>'admin', 'middleware'=>'is_admin'],function (){


    Route::get('home',function (){
        return view('admin.home');
    });

    Route::get('/home/{locale}',function ($locale){
        if(!in_array($locale,['en','bn'])){
            abort(400);
        }
        App::setLocale($locale);
        return view('admin.home');
    });

    /*------------------------- All Warranty Route ------------------------*/

    Route::get('addWarrantyProduct',[Controllers\WarrantyController::class,'addWarrantyProduct'])->name('add.warranty');
    Route::post('saveWarranty',[Controllers\WarrantyController::class,'saveWarranty'])->name('save.warranty');
    Route::get('allWarrantyProduct',[Controllers\WarrantyController::class,'getAllWarrantyProduct'])->name('all.warranty.product');
    Route::get('updateWarranty/{id}',[Controllers\WarrantyController::class,'updateWarranty'])->name('update.warranty');
    Route::post('updateWarrantyData',[Controllers\WarrantyController::class,'updateWarrantyData'])->name('save.update.warranty');
    Route::get('deleteWarranty/{id}',[Controllers\WarrantyController::class,'deleteWarranty'])->name('delete.warranty');
    Route::get('warrantyDetails/{id}',[Controllers\WarrantyController::class,'warrantyDetails'])->name('warranty.details');

    /*--------------------------- File Upload Route -------------------------*/

    Route::get('uploadForm',[Controllers\FileUploadController::class,'uploadForm'])->name('upload.form');
    Route::post('uploadFile',[Controllers\FileUploadController::class,'uploadFile'])->name('upload.file');

    /*---------------------------- Send Mail ----------------------------------*/

    Route::get('sendMail',[Controllers\MailController::class,'sendMail'])->name('send.mail');

    Route::get('studentList',[Controllers\StudentController::class,'index'])->name('student.list');
    Route::post('addStudent',[Controllers\StudentController::class,'saveStudent'])->name('save.student');

    Route::get('student/{id}',[Controllers\StudentController::class,'getStudentById']);
    Route::put('updateStudent',[Controllers\StudentController::class,'updateStudent'])->name('update.student');
    Route::get('deleteStudent/{id}',[Controllers\StudentController::class,'deleteStudent']);
    Route::delete("selectedStudents",[Controllers\StudentController::class,'deleteCheckedStudent'])->name('student.deleteSelected');

    Route::get('employee',[Controllers\EmployeeController::class,'index'])->name('employee');
    Route::get('addEmployee',[Controllers\EmployeeController::class,'addEmployee'])->name('add.employee');
    Route::post('saveEmployee',[Controllers\EmployeeController::class,'storeEmployee'])->name('store.employee');
    Route::get('editEmployee/{id}',[Controllers\EmployeeController::class,'editEmployee'])->name('edit.employee');
    Route::post('updateEmployee',[Controllers\EmployeeController::class,'updateEmployee'])->name('update.employee');
    Route::get('deleteEmployee/{id}',[Controllers\EmployeeController::class,'deleteEmployee'])->name('delete.employee');
    Route::get('employee/{id}',[Controllers\EmployeeController::class,'viewEmployee'])->name('view.employee');
    Route::get('employeeDetails/{id}',[Controllers\EmployeeController::class,'employeeDetails']);

    Route::get('download-excel',[Controllers\EmployeeController::class,'exportIntoExcel'])->name('employee.list.excel');
    Route::get('download-csv',[Controllers\EmployeeController::class,'exportInCsv'])->name('employee.list.csv');
    Route::get('pdfView',[Controllers\EmployeeController::class,'pdfView']);
    Route::get('download-pdf',[Controllers\EmployeeController::class,'downloadPdf'])->name('employee.list.pdf');
    Route::post('employee-import',[Controllers\EmployeeController::class,'importEmployeeFromExcelCsv'])->name('employee.import');

    /*-------------------------Datatables CRUD-----------------------------------*/

    //Route::resource('companies',[Controllers\CompanyCRUDController::class]);
    //Route::post('delete-company',[Controllers\CompanyCRUDController::class,'destroy']);


    /*-----------Auto complete search -------------------------*/

    Route::get('search',[Controllers\ProductController::class,'productList'])->name('search');
    Route::get('getProductName',[Controllers\ProductController::class,'getProduct'])->name('product.name');
    Route::get('addProduct',[Controllers\ProductController::class,'addProduct'])->name('add.product');


    /*---------Contact form -------------*/

    Route::get('contactUs',[Controllers\ContactController::class,'contactUs'])->name('contact.us');
    Route::post('sendMail',[Controllers\ContactController::class,'sendMail'])->name('send.mail');

    /*---------Make a  Zip File ---------*/

    Route::get('zip',[Controllers\ZipController::class,'createZip']);

    /*--------Use Helpers-------------------*/

    Route::get('helpers',[Controllers\HelpersController::class,'getName']);

    /*--------Yajra Datatable---------------*/

    Route::get('customers',[Controllers\CustomerController::class,'index'])->name('customers');
    Route::get('customer/list',[Controllers\CustomerController::class,'getCustomer'])->name('customer.list');


    /*------- use lazy loader-------*/

    Route::get('gallery',[Controllers\GalleryController::class,'index']);

    /*----Infinite Scroll Pagination----*/

    Route::get('posts',[Controllers\PostController::class,'index']);
    Route::get('autoload',[Controllers\PostController::class,'autoload']);

    Route::get('barChart',[Controllers\HomeController::class,'barChart']);

    /*---------Multi part registration form ----------*/

    Route::get('registration',[Controllers\MultiPartFormController::class,'index']);
    Route::post('submitreg',[Controllers\MultiPartFormController::class,'registration'])->name('registration.form');

    /* ------ Ajax image upload -------------------*/

    Route::get('image-preview', [Controllers\AjaxUploadController::class, 'index']);
    Route::post('upload', [Controllers\AjaxUploadController::class, 'store']);




    Route::get('ajaxImageUpload', [AjaxImageUploadController::class, 'ajaxImageUpload']);
    Route::post('ajaxImageUpload', [AjaxImageUploadController::class, 'ajaxImageUploadPost'])->name('ajaxImageUpload');

    /*--------Repository patern */

    Route::get('student_list',[Controllers\StudentRepositoryController::class,'index'])->name('student.list');
    Route::post('add_student',[Controllers\StudentRepositoryController::class,'saveStudent'])->name('save.student');

    Route::get('student/{id}',[Controllers\StudentRepositoryController::class,'getStudentById']);
    Route::put('update_student',[Controllers\StudentRepositoryController::class,'updateStudent'])->name('update.student');
    Route::get('delete_student/{id}',[Controllers\StudentRepositoryController::class,'deleteStudent']);
    Route::delete("selected_students",[Controllers\StudentRepositoryController::class,'deleteCheckedStudent'])->name('student.deleteSelected');

    /*---------Vendor Controller------*/

    Route::get('/vendor-list',[Controllers\VendorController::class,'index']);

});




