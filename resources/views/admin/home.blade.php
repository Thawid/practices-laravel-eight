@extends('admin.layouts.master')
@section('body')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"> {{ __('message.Dashboard') }} </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/home')}}">{{__('message.Home')}}</a></li>
                            <li class="breadcrumb-item active">{{ __('message.Dashboard') }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-signal"></i></span>
                            <div class="info-box-content">
                                <div class="name"><strong style="color: #17a2b8">Revenue</strong></div>
                                <div class="count-number revenue-data" style="">0.00</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-danger elevation-1"><img src="{{asset('dist/img/arrow-return-left.png')}}" alt=""></span>
                            <div class="info-box-content">
                                <div class="name"><strong style="color: #dc3545">Sale Return</strong></div>
                                <div class="count-number return-data" style="">0.00</div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix hidden-md-up"></div>
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1"><img src="{{asset('dist/img/arrow-return-right.png')}}" alt=""></span>

                            <div class="info-box-content">
                                <div class="name"><strong style="color: #00c689">Purchase Return</strong></div>
                                <div class="count-number purchase_return-data" style="">0.00</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-chart-pie" style="color:#ffffff"></i></span>

                            <div class="info-box-content">
                                <div class="name"><strong style="color: #297ff9">Profit</strong></div>
                                <div class="count-number profit-data" style="">0.00</div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="row">
                    <div class="col-md-6">
                        <div class="card card-default">
                            <div class="card-header">
                                <h3 class="card-title">March 2020</h3>
                            </div>
                            <div class="card-body">
                                <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-default">
                            <div class="card-header">
                                <h3 class="card-title">Purchase & Sales Chart</h3>
                            </div>
                            <div class="card-body">
                                <div class="chart">
                                    <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <h4>High Chart For Posts</h4>
                            </div>
                            <div class="card-body">
                                <div id="post-chart"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <h4>Bar Chart For Posts</h4>
                            </div>
                            <div class="card-body">
                                <canvas id="barChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <h4>Recent Five Transaction</h4>
                            </div>
                            <div class="card-body">
                                <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-content-below-sale-tab" data-toggle="pill" href="#custom-content-below-sale" role="tab" aria-controls="custom-content-below-sale" aria-selected="true">Sale</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-content-below-purchase-tab" data-toggle="pill" href="#custom-content-below-purchase" role="tab" aria-controls="custom-content-below-purchase" aria-selected="false">Purchase</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-content-below-quotation-tab" data-toggle="pill" href="#custom-content-below-quotation" role="tab" aria-controls="custom-content-below-quotation" aria-selected="false">Quotation</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-content-below-payment-tab" data-toggle="pill" href="#custom-content-below-payment" role="tab" aria-controls="custom-content-below-payment" aria-selected="false">Payment</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="custom-content-below-tabContent">
                                    <div class="tab-pane fade show active" id="custom-content-below-sale" role="tabpanel" aria-labelledby="custom-content-below-sale-tab">
                                        <div class="table-responsive mt-4">
                                            <table id="" class="table  sale" style="width:100%">
                                                <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Reference</th>
                                                    <th>Customer</th>
                                                    <th>Grand total</th>
                                                    <th>status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>09/01/2020 14:55:23</td>
                                                    <td>sr-20200109-025523</td>
                                                    <td>Md Tanvirul Islam</td>
                                                    <td>440.00</td>
                                                    <td>
                                                        <div class="badge badge-success">Completed</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>09/01/2020 14:55:23</td>
                                                    <td>sr-20200109-025523</td>
                                                    <td>Md Tanvirul Islam</td>
                                                    <td>440.00</td>
                                                    <td>
                                                        <div class="badge badge-success">Completed</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Reference</th>
                                                    <th>Customer</th>
                                                    <th>Grand total</th>
                                                    <th>status</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="custom-content-below-purchase" role="tabpanel" aria-labelledby="custom-content-below-purchase-tab">
                                        <div class="table-responsive mt-4">
                                            <table id="" class="table purchase" style="width:100%">
                                                <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Reference</th>
                                                    <th>Supplier</th>
                                                    <th>Grand total</th>
                                                    <th>status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>09/01/2020 14:55:23</td>
                                                    <td>sr-20200109-025523</td>
                                                    <td>Md Tanvirul Islam</td>
                                                    <td>440.00</td>
                                                    <td>
                                                        <div class="badge badge-success">Completed</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>09/01/2020 14:55:23</td>
                                                    <td>sr-20200109-025523</td>
                                                    <td>Md Tanvirul Islam</td>
                                                    <td>440.00</td>
                                                    <td>
                                                        <div class="badge badge-success">Completed</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Reference</th>
                                                    <th>Supplier</th>
                                                    <th>Grand total</th>
                                                    <th>status</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="custom-content-below-quotation" role="tabpanel" aria-labelledby="custom-content-below-quotation-tab">
                                        <div class="table-responsive mt-4">
                                            <table id="" class="table quotation" style="width:100%">
                                                <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Reference</th>
                                                    <th>Customer</th>
                                                    <th>Supplier</th>
                                                    <th>Grand total</th>
                                                    <th>status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>09/01/2020 14:55:23</td>
                                                    <td>sr-20200109-025523</td>
                                                    <td>Md Tanvirul Islam</td>
                                                    <td>Md Tanvirul Islam</td>

                                                    <td>5000.00</td>
                                                    <td>
                                                        <div class="badge badge-success">Sent</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>09/01/2020 14:55:23</td>
                                                    <td>sr-20200109-025523</td>
                                                    <td>Md Tanvirul Islam</td>
                                                    <td>Md Tanvirul Islam</td>

                                                    <td>5000.00</td>
                                                    <td>
                                                        <div class="badge badge-danger">Pending</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th colspan="4" class="text-right">Total : </th>
                                                    <th>1000.00</th>

                                                    <th></th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="custom-content-below-payment" role="tabpanel" aria-labelledby="custom-content-below-payment-tab">
                                        <div class="table-responsive mt-4">
                                            <table id="" class="table payment" style="width:100%">
                                                <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Reference</th>
                                                    <th>Amount</th>
                                                    <th>Paid By</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>09/01/2020 14:55:23</td>
                                                    <td>sr-20200109-025523</td>
                                                    <td>5000.00</td>
                                                    <td>Credit Card</td>
                                                </tr>
                                                <tr>
                                                    <td>09/01/2020 14:55:23</td>
                                                    <td>sr-20200109-025523</td>
                                                    <td>5000.00</td>
                                                    <td> Cash </td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Reference</th>
                                                    <th>Amount</th>
                                                    <th>Paid By</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>

@endsection
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js" integrity="sha512-vBmx0N/uQOXznm/Nbkp7h0P1RfLSj0HQrFSzV8m7rOGyj30fYAOKHYvCNez+yM8IrfnW0TCodDEjRqf6fodf/Q==" crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript">
        var postData = <?php echo json_encode($highChartData)?>;

        Highcharts.chart('post-chart', {
            title: {
                text: 'New Post Growth, 2021'
            },
            subtitle: {
                text: 'Laravel Eight'
            },
            xAxis: {
                categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                    'October', 'November', 'December'
                ]
            },
            yAxis: {
                title: {
                    text: 'Number of New Posts'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            plotOptions: {
                series: {
                    allowPointSelect: true
                }
            },
            series: [{
                name: 'New Post',
                data: postData
            }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });

    </script>
    <script>
        $(function (){
            var barChartData = <?php echo json_encode($barChartData); ?>;
            var barCanvas = $("#barChart");
            var barChart = new Chart(barCanvas,{
                type:'bar',
                data:{
                    labels:['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep','Oct', 'Nov', 'Dec'],
                    datasets:[{
                        label:'New Post, 2021',
                        data:barChartData,
                        backgroundColor:['red','green','blue','yellow','orange','indigo','yellow','pink','red','ass','green','orange'],
                    }]
                },
                options:{
                    scales:{
                        yAxes:[{
                            ticks:{
                                beginAtZero:true
                            }
                        }]
                    }
                }

            });
        })
    </script>
    {{--<script>
        $(function () {
            /* ChartJS
             * -------
             * Here we will create a few charts using ChartJS
             */
            //-------------
            //-------------
            //- BAR CHART -
            //-------------
            var areaChartData = {
                labels  : ['January', 'February', 'March', 'April', 'May', 'June'],
                datasets: [
                    {
                        label               : 'Purchase',
                        backgroundColor     : 'rgba(60,141,188,0.9)',
                        borderColor         : 'rgba(60,141,188,0.8)',
                        pointRadius          : false,
                        pointColor          : '#3b8bba',
                        pointStrokeColor    : 'rgba(60,141,188,1)',
                        pointHighlightFill  : '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data                : [28, 48, 40, 19, 86, 27, 90]
                    },
                    {
                        label               : 'Sales',
                        backgroundColor     : 'rgba(210, 214, 222, 1)',
                        borderColor         : 'rgba(210, 214, 222, 1)',
                        pointRadius         : false,
                        pointColor          : 'rgba(210, 214, 222, 1)',
                        pointStrokeColor    : '#c1c7d1',
                        pointHighlightFill  : '#fff',
                        pointHighlightStroke: 'rgba(220,220,220,1)',
                        data                : [65, 59, 80, 81, 56, 55, 40]
                    },
                ]
            }
            var barChartCanvas = $('#barChart').get(0).getContext('2d')
            var barChartData = jQuery.extend(true, {}, areaChartData)
            var temp0 = areaChartData.datasets[0]
            var temp1 = areaChartData.datasets[1]
            barChartData.datasets[0] = temp1
            barChartData.datasets[1] = temp0

            var barChartOptions = {
                responsive              : true,
                maintainAspectRatio     : false,
                datasetFill             : false
            }

            var barChart = new Chart(barChartCanvas, {
                type: 'bar',
                data: barChartData,
                options: barChartOptions
            })

            //-------------
            //- DONUT CHART -
            //-------------
            // Get context with jQuery - using jQuery's .get() method.

            var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
            var donutData        = {
                labels: [
                    'Stock Value By Price',
                    'Stock Value By Cost',
                    'Estimate Profite',
                ],
                datasets: [
                    {
                        data: [700,500,200],
                        backgroundColor : ['#00c0ef', '#3c8dbc', '#d2d6de'],
                    }
                ]
            }
            var donutOptions     = {
                maintainAspectRatio : false,
                responsive : true,
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            var donutChart = new Chart(donutChartCanvas, {
                type: 'doughnut',
                data: donutData,
                options: donutOptions
            })

        })
    </script>--}}
    <script>

        @if(Session::has('mail-send'))
            toastr.success("{{ Session::get('mail-send') }}");
        @endif

    </script>
  @endsection
