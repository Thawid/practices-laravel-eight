@extends('admin.layouts.master')
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
@endsection
@section('body')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Update Warranty </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Update Warranty</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <p><em>The field labels marked with * are required input fields.</em></p>
                            </div>

                            <div class="card-body">
                                @if(session()->has('status'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <button  class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        {{  session()->get('status') }}
                                    </div>
                                @endif
                                <form action="{{route('save.update.warranty')}}" method="POST">
                                    @csrf
                                    <div class="row">
                                        <input type="hidden" name="id" value="{{$update->id}}">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="invoice_id">Invoice ID *</label>
                                                    <input type="text" id="invoice_id" name="invoice_id" class="form-control" value="{{$update->invoice_id}}">
                                                @error('invoice_id')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="product_name">Product Name *</label>
                                                <input type="text" id="product_name" name="product_name" class="form-control" value="{{$update->product_name}}">
                                                @error('product_name')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="product_serial_no">Product Serial No</label>
                                                <input type="text" id="product_serial_no" name="product_serial_no" class="form-control" value="{{$update->product_serial_no}}">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="receiving_date">Receiving Date *</label>
                                                <input type="date" id="receiving_date" name="receiving_date" class="form-control" value="{{$update->receiving_date}}">
                                                @error('receiving_date')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="delivery_date">Delivery Date *</label>
                                                <input type="date" id="delivery_date" name="delivery_date" class="form-control" value="{{$update->delivery_date}}">
                                                @error('receiving_date')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="warranty_type">Warranty Type *</label>
                                                <select name="warranty_type" id="warranty_type" class="form-control select2"
                                                        style="width: 100%;">
                                                    <option @if($update->warranty_type == 'Replacement') selected @endif value="Replacement" >Replacement</option>
                                                    <option @if($update->warranty_type == 'Repair') selected @endif value="Repair">Repair</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="status">Status *</label>
                                                <select name="status" id="status" class="form-control select2"
                                                        style="width: 100%;">
                                                    <option @if($update->status == 'Delivered') selected @endif value="Delivered" >Delivered</option>
                                                    <option @if($update->status == 'Pending') selected @endif value="Pending">Pending</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label for="description">Description *</label>
                                                <textarea class="form-control" name="description" id="description" cols="30" rows="1">{{$update->description}}</textarea>
                                                @error('description')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="submit" class="btn btn-primary" value="Submit"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
    <script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script>
        $(function () {
             $('.select2').select2()

        });

    </script>
    <script>

        @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
        @endif

    </script>

@endsection
