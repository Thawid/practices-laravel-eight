
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">{{$warranty->product_name}}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <table class="table table-bordered table-responsive">
            <thead>

            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Invoice ID</td>
                <td>{{ $warranty->invoice_id }}</td>

            </tr>
            <tr>
                <td>2.</td>
                <td>Product Code</td>
                <td>{{ $warranty->product_serial_no }}</td>

            </tr>
            <tr>
                <td>3.</td>
                <td>R. Date</td>
                <td>{{ $warranty->receiving_date }}</td>

            </tr>
            <tr>
                <td>4.</td>
                <td>D. Date</td>
                <td>{{ $warranty->delivery_date }}</td>

            </tr>
            <tr>
                <td>5.</td>
                <td>Warranty Type</td>
                <td>{{ $warranty->warranty_type }}</td>

            </tr>
            <tr>
                <td>6.</td>
                <td>Status</td>
                <td>{{ $warranty->status }}</td>

            </tr>
            <tr>
                <td>7.</td>
                <td>Description</td>
                <td>{{ $warranty->description }}</td>

            </tr>
            </tbody>
        </table>
    </div>
</div>


