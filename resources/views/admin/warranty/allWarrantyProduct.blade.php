@extends('admin.layouts.master')
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('body')
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Warranty List </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/admin/home')}}">Home</a></li>
                        <li class="breadcrumb-item active">Warranty List</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('add.warranty')}}" type="button" class="btn btn-primary">
                                    <i class="fas fa-plus-square"> </i> Add Warranty
                                </a>
                            </h3>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive p-0">
                                <table id="warrantyList" class="table table-bordered table-striped dataTable" role="grid">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th> Invoice ID</th>
                                        <th>Product Name</th>
                                        <th>Serial No</th>
                                        <th>Receive</th>
                                        <th>Delivery</th>
                                        <th>W. Type</th>
                                        <th>Status</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($allWarrantyProduct as $allWarranty)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td> {{$allWarranty->invoice_id}}</td>
                                        <td> {{$allWarranty->product_name}}</td>
                                        <td> {{$allWarranty->product_serial_no}}</td>
                                        <td> {{$allWarranty->receiving_date}}</td>
                                        <td> {{$allWarranty->delivery_date}}</td>
                                        <td> {{$allWarranty->warranty_type}}</td>
                                        <td> {{$allWarranty->status}}</td>
                                        <td> {{$allWarranty->description}}</td>
                                        <td>
                                            <div class="input-group-prepend">
                                                <button type="button" class="btn btn-default dropdown-toggle"
                                                        data-toggle="dropdown" aria-expanded="false">
                                                    Action
                                                </button>
                                                <div class="dropdown-menu" x-placement="bottom-start"
                                                     style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                                                    <a class="dropdown-item" href="{{route('update.warranty',$allWarranty->id)}}"> <i
                                                            class="fas fa-edit"> </i> Edit</a>
                                                    <a class="dropdown-item"  href="{{route('delete.warranty',$allWarranty->id)}}">
                                                        <i
                                                            class="fas fa-trash"> </i> Delete</a>
                                                    <a href="javascript:void(0);" class="dropdown-item wdetails"
                                                            data-toggle="modal"
                                                            data-target="#warrantyDetails"
                                                            data-id="{{$allWarranty->id}}">
                                                       <i class="fas fa-eye"></i> View
                                                    </a>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>SL</th>
                                        <th> Invoice ID</th>
                                        <th>Product Name</th>
                                        <th>Serial No</th>
                                        <th>Receive</th>
                                        <th>Delivery</th>
                                        <th>W. Type</th>
                                        <th>Status</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('modal')
    <div class="modal fade" id="warrantyDetails">
        <div class="modal-dialog">
            <div id="warranty-data"> </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
        $(function () {

            $('#warrantyList').DataTable({
                "processing":true,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });

    </script>
    <script>
        $(document).ready(function () {

            $(".wdetails").click(function (e) {
                $currID = $(this).attr("data-id");
                $.get("warrantyDetails/"+$currID, function (data) {
                        console.log(data);
                        $('#warranty-data').html(data);
                    }
                );
            });
        });
    </script>
    <script>
        @if(Session::has('status'))
        toastr.success("{{ Session::get('status') }}");
        @endif
        @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
        @endif
    </script>

    @endsection
