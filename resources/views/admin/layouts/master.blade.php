<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title> Laravel-8</title>

    @yield('style')
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">

    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Custom CSS File -->
    <link rel="stylesheet" href="{{asset('dist/style.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>

        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a class="btn btn-block btn-outline-primary" href="javascript:void(0);"><i class="fa fa-shopping-bag"></i><span> POS</span></a></li>
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-danger navbar-badge">15</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <a href="#" class="dropdown-item dropdown-footer">15 product exceeds alert quantity</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a rel="nofollow" data-target="#" href="#" data-toggle="dropdown" class="nav-link"><span>Language</span> <i class="fa fa-angle-down"></i></a>
                <ul class="dropdown-menu dropdown-menu-right" user="menu">
                    <li>
                        <a href="{{url('admin/home/en')}}" class="dropdown-item"> English</a>
                    </li>
                    <li>
                        <a href="{{url('admin/home/bn')}}" class="dropdown-item"> Bangla</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a rel="nofollow" data-target="#" href="#" data-toggle="dropdown" class="nav-link"><i  class="fa fa-user"></i> <span>{{\Illuminate\Support\Facades\Auth::user()->name}}</span> <i class="fa fa-angle-down"></i></a>
                <ul class="dropdown-menu dropdown-menu-right" user="menu">
                    <li>
                        <a href="UserProfile.php" class="dropdown-item"><i class="fa fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="GeneralSetting.php" class="dropdown-item"><i class="fa fa-cog"></i> Settings</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="dropdown-item"><i class="fa fa-database"></i> DB Backup</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i> {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
                        class="fas fa-th-large"></i></a>
            </li>
        </ul>
    </nav>

@extends('admin.layouts.navbar')
@yield('body')

@yield('modal')
<aside class="control-sidebar control-sidebar-dark">

</aside>
<footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="http://winnerdevs.com">WinnerDevs</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0
    </div>
</footer>
</div>

<!-- jQuery -->

<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- InputMask -->
<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>

<!-- Bootstrap Switch -->
<script src="{{asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>



<!-- Toastr -->
<script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.js') }}"></script>
<!-- OPTIONAL SCRIPTS -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('../plugins/chart.js/Chart.min.js')}}"></script>
@yield('script')
</body>
</html>


