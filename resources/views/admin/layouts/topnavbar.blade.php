<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>

        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a class="btn btn-block btn-outline-primary" href="javascript:void(0);"><i class="fa fa-shopping-bag"></i><span> POS</span></a></li>
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-danger navbar-badge">15</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <a href="#" class="dropdown-item dropdown-footer">15 product exceeds alert quantity</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a rel="nofollow" data-target="#" href="#" data-toggle="dropdown" class="nav-link"><span>Language</span> <i class="fa fa-angle-down"></i></a>
                <ul class="dropdown-menu dropdown-menu-right" user="menu">
                    <li>
                        <a href="javascript:void(0);" class="dropdown-item"> English</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="dropdown-item"> Bangla</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a rel="nofollow" data-target="#" href="#" data-toggle="dropdown" class="nav-link"><i  class="fa fa-user"></i> <span>Admin</span> <i class="fa fa-angle-down"></i></a>
                <ul class="dropdown-menu dropdown-menu-right" user="menu">
                    <li>
                        <a href="UserProfile.php" class="dropdown-item"><i class="fa fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="GeneralSetting.php" class="dropdown-item"><i class="fa fa-cog"></i> Settings</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="dropdown-item"><i class="fa fa-database"></i> DB Backup</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i> {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
                        class="fas fa-th-large"></i></a>
            </li>
        </ul>
    </nav>
