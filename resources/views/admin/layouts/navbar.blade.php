<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{url('admin/home')}}" class="brand-link">
        <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="QPOS" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">QUICK POS</span>
    </a>
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview menu-open">
                    <a href="{{url('/admin/home')}}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p> Dashboard </p>
                    </a>

                </li>
                @if(auth()->user()->is_admin == 1)
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-retweet nav-icon"></i>
                        <p>
                            Warranty
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('add.warranty')}}" class="nav-link">
                                <i class="fas fa-arrow-right nav-icon"></i>
                                <p>Add Warranty</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('all.warranty.product')}}" class="nav-link">
                                <i class="fas fa-arrow-right nav-icon"></i>
                                <p>Warranty List</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{route('upload.form')}}" class="nav-link">
                        <i class="nav-icon fas fa-retweet nav-icon"></i>
                        <p> File Upload </p>
                    </a>

                </li>

                <li class="nav-item has-treeview">
                    <a href="{{route('send.mail')}}" class="nav-link">
                        <i class="nav-icon fas fa-retweet nav-icon"></i>
                        <p> Send Mail </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{route('student.list')}}" class="nav-link">
                        <i class="nav-icon fas fa-retweet nav-icon"></i>
                        <p> Student(Ajax) </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-retweet nav-icon"></i>
                        <p>
                            Employee
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('add.employee')}}" class="nav-link">
                                <i class="fas fa-arrow-right nav-icon"></i>
                                <p>Add Employee</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('employee')}}" class="nav-link">
                                <i class="fas fa-arrow-right nav-icon"></i>
                                <p>Employee List</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="{{route('search')}}" class="nav-link">
                        <i class="nav-icon fas fa-retweet nav-icon"></i>
                        <p> Autocomplete Search </p>
                    </a>
                </li>

                <li class="nav-item has-treeview">
                    <a href="{{route('contact.us')}}" class="nav-link">
                        <i class="nav-icon fas fa-retweet nav-icon"></i>
                        <p> Contact Form </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{route('customers')}}" class="nav-link">
                        <i class="nav-icon fas fa-retweet nav-icon"></i>
                        <p> Customer List(YDT) </p>
                    </a>
                </li>

                <li class="nav-item has-treeview">
                    <a href="{{url('admin/posts')}}" class="nav-link">
                        <i class="nav-icon fas fa-retweet nav-icon"></i>
                        <p> Post List(Infinite Scroll) </p>
                    </a>
                </li>


                @endif

            </ul>
        </nav>
    </div>
</aside>
