<!DOCKTYPE html>
<html lang="en">
<head>
    <meta CHARSET="UTF-8">
    <title>Contact Mail</title>
</head>
<body>
    <h5>Mail Details</h5>
    <p>Name : {{$details['name']}}</p>
    <p>E-mail : {{$details['email']}}</p>
    <p>Phone : {{$details['phone']}}</p>
    <p>Message : {{$details['description']}}</p>
</body>
</html>
