<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employee List</title>
    <style>
        .table{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        .table td, .table th{
            border:1px solid #9B9FA2;
            padding:3px;
            text-align: center;
        }

        .table tr:nth-child(even){
            background-color: #7a8793;
        }

        .table th{
            padding-top:12px;
            padding-bottom: 12px;
            text-align: center;
            background-color:#7a8793;
        }
    </style>
</head>
<body>
<table id="employeeList" class="table" role="grid">
    <thead>
    <tr>
        <th>SL</th>
        <th>Image</th>
        <th> Employee Name</th>
        <th> Designation</th>
        <th> Department</th>
        <th> E-Mail</th>
        <th> Phone</th>
    </tr>
    </thead>
    <tbody>
    @foreach($employees as $employee)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td><img src="{{asset('employee-profile-pic/'.$employee->profile_picture)}}" alt="" style="max-width: 50px;"> </td>
            <td> {{$employee->employee_name}}</td>
            <td> {{$employee->designation}}</td>
            <td> {{$employee->department}}</td>
            <td> {{$employee->email}}</td>
            <td> {{$employee->phone}}</td>
        </tr>
    @endforeach
    </tbody>

</table>
</body>
</html>



