@extends('admin.layouts.master')
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
    <style>
        #employeeList{
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
@endsection
@section('body')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Employee List </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('/admin/home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Employee List</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <a href="{{route('add.employee')}}" type="button" class="btn btn-primary">
                                        <i class="fas fa-plus-square"> </i> Add Employee
                                    </a>
                                    <a href="#" type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadFile">
                                        <i class="fas fa-upload"> </i> Import File
                                    </a>

                                    <a href="{{route('employee.list.excel')}}" type="button" class="btn btn-primary">
                                        <i class="fas fa-download"> </i> Excel
                                    </a>
                                    <a href="{{route('employee.list.csv')}}" type="button" class="btn btn-primary">
                                        <i class="fas fa-download"> </i> CSV
                                    </a>
                                    <a href="{{route('employee.list.pdf')}}" type="button" class="btn btn-primary">
                                        <i class="fas fa-download"> </i> pdf
                                    </a>

                                </h3>
                            </div>
                            <div class="card-body">

                                <div class="table-responsive p-0">
                                    <table id="employeeList" class="table table-bordered table-striped dataTable" role="grid">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Image</th>
                                            <th> Employee Name</th>
                                            <th> Designation</th>
                                            <th> Department</th>
                                            <th> E-Mail</th>
                                            <th> Phone</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($employees as $employee)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td><img src="{{asset('employee-profile-pic/'.$employee->profile_picture)}}" alt="" style="max-width: 50px;"> </td>
                                                <td> {{$employee->employee_name}}</td>
                                                <td> {{$employee->designation}}</td>
                                                <td> {{$employee->department}}</td>
                                                <td> {{$employee->email}}</td>
                                                <td> {{$employee->phone}}</td>

                                                <td>
                                                    <div class="input-group-prepend">
                                                        <button type="button" class="btn btn-default dropdown-toggle"
                                                                data-toggle="dropdown" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" x-placement="bottom-start"
                                                             style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                                                            <a class="dropdown-item" href="{{route('edit.employee',$employee->id)}}"> <i
                                                                    class="fas fa-edit"> </i> Edit</a>
                                                            <a class="dropdown-item"  href="{{route('delete.employee',$employee->id)}}">
                                                                <i
                                                                    class="fas fa-trash"> </i> Delete</a>
                                                            <a class="dropdown-item" href="javascript:void(0);" onclick="viewEmployee({{$employee->id}});">
                                                                <i class="fas fa-eye"></i> View
                                                            </a>

                                                            <a href="javascript:void(0);" class="dropdown-item empdetails"
                                                               data-toggle="modal"
                                                               data-target="#employeeDetails"
                                                               data-id="{{$employee->id}}">
                                                                <i class="fas fa-eye"></i> View
                                                            </a>

                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>SL</th>
                                            <th>Image</th>
                                            <th> Employee Name</th>
                                            <th> Designation</th>
                                            <th> Department</th>
                                            <th> E-Mail</th>
                                            <th> Phone</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="viewEmployee">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Employee Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <div class="card-body">
                        <div class="form-group">
                            <label for="employee_name">Employee Name</label>
                            <input type="text" class="form-control" id="employee_name" name="employee_name">
                        </div>
                        <div class="form-group">
                            <label for="designation">Designation</label>
                            <input type="text" class="form-control" id="designation" name="designation">
                        </div>
                        <div class="form-group">
                            <label for="department">Department</label>
                            <input type="text" class="form-control" id="department" name="department">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="tel" class="form-control" id="phone" name="phone">
                        </div>
                        <div class="form-group">
                            <label for="phone">Profile Picture</label>
                            <img id="previewImg" name="profile_picture" alt="Profile Picture" style="max-width: 130px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="employeeDetails">
        <div class="modal-dialog">
            <div id="employee-data"> </div>
        </div>
    </div>
    <div class="modal fade" id="uploadFile">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload Excel/CSV File</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="" method="POST" action="{{route('employee.import')}}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" id="id" name="id">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="file">Upload File</label>
                                <input type="file" class="form-control" id="file" name="file">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('script')
    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    e
    <script>
        $(function () {

            $('#employeeList').DataTable({
                "processing":true,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });

    </script>
    <script>
        $(document).ready(function () {

            $(".empdetails").click(function (e) {
                $currID = $(this).attr("data-id");
                $.get("employeeDetails/"+$currID, function (data) {
                        console.log(data);
                        $('#employee-data').html(data);
                    }
                );
            });
        });
    </script>
    <script>
        function previewFile(input) {
            var file = $("input[type=file]").get(0).files[0];
            if(file){
                var reader = new FileReader();
                reader.onload = function () {
                    $("#previewImg").attr("src",reader.result);
                }
                reader.readAsDataURL(file);
            }
        }
    </script>
    <script>
        function viewEmployee(id) {
            $.get('employee/'+id,function (employee) {
                $("#employee_name").val(employee.employee_name);
                $("#designation").val(employee.designation);
                $("#department").val(employee.department);
                $("#email").val(employee.email);
                $("#phone").val(employee.phone);
                $("#previewImg").val(employee.profile_picture);
                $("#viewEmployee").modal('toggle');
            });
        }
    </script>
    <script>
    @if(Session::has('add-employee'))
    toastr.success("{{ Session::get('add-employee') }}");
    @endif
    @if(Session::has('delete-message'))
        toastr.success("{{Session::get('delete-message')}}")
    @endif
    @if(Session::has('error'))
    toastr.error("{{ Session::get('error') }}");
    @endif
    </script>
    @if(Session::has('add-employee'))
        <script>
            swal("Great Job!","{!! Session::get('add-employee') !!}","success",{
                button:"OK",
            })
        </script>
    @endif

    @if(Session::has('import-file'))
        <script>
            swal("Great Job!","{!! Session::get('import-file') !!}","success",{
                button:"OK",
            })
        </script>
    @endif




@endsection
