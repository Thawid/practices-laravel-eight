@extends('admin.layouts.master')
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
@endsection
@section('body')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Update Employee </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Update Warranty</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <p><em>The field labels marked with * are required input fields.</em></p>
                            </div>

                            <div class="card-body">
                                @if(session()->has('message'))
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        {{  session()->get('message') }}
                                    </div>
                                @endif
                                <form action="{{route('update.employee')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <input type="hidden" name="id" value="{{$employee->id}}">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="employee_name">Employee Name *</label>
                                                <input type="text" id="employee_name" name="employee_name" class="form-control" value="{{$employee->employee_name}}">
                                                @error('employee_name')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="designation">Designation *</label>
                                                <input type="text" id="designation" name="designation" class="form-control" value="{{$employee->designation}}">
                                                @error('designation')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="department">Department</label>
                                                <input type="text" id="department" name="department" class="form-control" value="{{$employee->department}}">
                                                @error('department')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="email">E-Mail *</label>
                                                <input type="email" id="email" name="email" class="form-control" value="{{$employee->email}}">
                                                @error('email')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="phone">Phone *</label>
                                                <input type="tel" id="phone" name="phone" class="form-control" value="{{$employee->phone}}">
                                                @error('phone')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="profile_picture">Profile Picture *</label>
                                                <input type="file" id="profile_picture" name="profile_picture" class="form-control" onchange="previewFile(this)">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="phone">Image Preview </label>
                                                <img id="previewImg" src="{{asset('employee-profile-pic/'.$employee->profile_picture)}}" alt="Profile Picture" style="max-width: 130px;">
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <input type="submit" class="btn btn-primary" value="Submit"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
    <script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script>
        $(function () {
            $('.select2').select2()

        });

    </script>
    <script>
        function previewFile(input) {
            var file = $("input[type=file]").get(0).files[0];
            if(file){
                var reader = new FileReader();
                reader.onload = function () {
                    $("#previewImg").attr("src",reader.result);
                }
                reader.readAsDataURL(file);
            }
        }
    </script>
    <script>
        @if(Session::has('message'))
        toastr.success("{{ Session::get('message') }}");
        @endif
        @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
        @endif
    </script>
@endsection
