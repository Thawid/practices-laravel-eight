
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">{{$employee->employee_name}}</h4>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="card-body">
            <div class="form-group">
                <label for="designation">Designation</label>
                <input type="text" class="form-control" id="designation" name="designation" value="{{$employee->designation}}">
            </div>
            <div class="form-group">
                <label for="department">Department</label>
                <input type="text" class="form-control" id="department" name="department" value="{{$employee->department}}">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" value="{{$employee->email}}">
            </div>
            <div class="form-group">
                <label for="phone">Phone</label>
                <input type="tel" class="form-control" id="phone" name="phone" value="{{$employee->phone}}">
            </div>
            <div class="form-group">
                <label for="phone">Profile Picture</label>
                <img id="previewImg" name="profile_picture" src="{{asset('employee-profile-pic/'.$employee->profile_picture)}}" alt="Profile Picture" style="max-width: 130px;">
            </div>
        </div>
    </div>
</div>


