@extends('admin.layouts.master')
@section('style')

    <style>
        .form-section{
            padding-left: 15px;
            display: none;
        }

        .form-section.current{
            display: inherit;
        }
        .btn-info,.btn-success{
            margin-top: 10px;
        }
        .parsley-errors-list{
            margin: 2px 0 3px;
            padding: 0;
            list-style-type: none;
            color: red;
        }

    </style>
@endsection
@section('body')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Registration  </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Registration</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <p><em>The field labels marked with * are required input fields.</em></p>
                            </div>

                            <div class="card-body">
                                <form class="reg-form" action="{{route('registration.form')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 offset-md-3">
                                            <div class="form-section">
                                                <div class="form-group">
                                                    <label for="fname">First Name *</label>
                                                    <input type="text" class="form-control" name="fname" id="fname" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="lname">Last Name *</label>
                                                    <input type="text" class="form-control" name="lname" id="lname" required>
                                                </div>
                                            </div>
                                            <div class="form-section">
                                                <div class="form-group">
                                                    <label for="phone">Phone *</label>
                                                    <input type="text" class="form-control" name="phone" id="phone" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">E-mail *</label>
                                                    <input type="email" class="form-control" name="email" id="email" required>
                                                </div>
                                            </div>
                                            <div class="form-section">
                                                <label for="address">Address *</label>
                                                <textarea name="address" id="address" cols="30" rows="5" class="form-control" required></textarea>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-navigation">
                                                    <button type="button" class="previous btn btn-info float-left">Previous</button>
                                                    <button type="button" class="next btn btn-primary float-right">Next</button>
                                                    <button type="submit" class="next btn btn-success float-right">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
    <script>
        $(function(){

            var $section = $('.form-section');

            function navigateTo(index){
                $section.removeClass('current').eq(index).addClass('current');
                $('.form-navigation .previous').toggle(index>0);
                var atTheEnd = index >= $section.length -1;
                $('.form-navigation .next').toggle(!atTheEnd);
                $('.form-navigation [type=submit]').toggle(atTheEnd);
            }

            function curIndex(){
                return $section.index($section.filter('.current'));
            }

            $('.form-navigation .previous').click(function (){
                navigateTo(curIndex()-1);
            })

            $('.form-navigation .next').click(function (){
                $('.reg-form').parsley().whenValidate({
                    group:'block-' + curIndex()
                }).done(function (){
                    navigateTo(curIndex()+1);
                });
            });

            $section.each(function (index, section){
                $(section).find(':input').attr('data-parsley-group','block-'+index);
            });

            navigateTo(0);

        });


    </script>
@endsection
