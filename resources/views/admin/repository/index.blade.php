@extends('admin.layouts.master')
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('body')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Student List </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('/admin/home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Student List</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <a href="#" type="button" class="btn btn-primary" data-toggle="modal" data-target="#addStudent">
                                        <i class="fas fa-plus-square"> </i> Add Student
                                    </a>
                                    <a href="#" class="btn btn-danger" id="deleteSelectedRecord">Delete Selected</a>
                                </h3>
                            </div>
                            <div class="card-body">

                                <div class="table-responsive p-0">
                                    <table id="studentList" class="table table-bordered table-striped dataTable" role="grid">
                                        <thead>
                                        <tr>
                                            <th><input type="checkbox" id="checkAll"/></th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                       @foreach($students->chunk(1) as $data)
                                           @foreach($data as $row)

                                            <tr id="sid{{$row->id}}">
                                                <td><input type="checkbox" name="ids" class="checkBoxClass" value="{{$row->id}}"></td>
                                                <td> {{$row->first_name}}</td>
                                                <td> {{$row->last_name}}</td>
                                                <td> {{$row->email}}</td>
                                                <td> {{$row->phone}}</td>
                                                <td>
                                                    <div class="input-group-prepend">
                                                        <button type="button" class="btn btn-default dropdown-toggle"
                                                                data-toggle="dropdown" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" x-placement="bottom-start"
                                                             style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                                                            <a class="dropdown-item" href="javascript:void(0);" onclick="editStudent({{$row->id}});">
                                                                <i class="fas fa-edit"> </i> Edit</a>
                                                            <a class="dropdown-item" href="javascript:void(0);" onclick="deleteStudent({{$row->id}});">
                                                                <i class="fas fa-trash"> </i> Delete</a>

                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @if ($row->count() >= 10)
                                                @break
                                            @endif
                                        @endforeach
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('modal')
    {{--add student modal--}}
    <div class="modal fade" id="addStudent">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Student</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add_new_student">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" class="form-control" id="first_name" name="first_name">
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="form-control" id="last_name" name="last_name">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" class="form-control" id="phone" name="phone">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

{{--    update student modal--}}
    <div class="modal fade" id="updateStudent">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update  Student</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="update_student">
                        @csrf
                        <input type="hidden" id="id" name="id">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" class="form-control" id="firstName" name="first_name">
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="form-control" id="lastName" name="last_name">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="e-mail" name="email">
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" class="form-control" id="phone_no" name="phone">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script>
        $(function () {

            $('#studentList').DataTable({
                "processing":true,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
            });
        });

    </script>
    <script>
        $('#add_new_student').submit(function (e) {
            e.preventDefault();

            let first_name = $("#first_name").val();
            let last_name = $("#last_name").val();
            let email = $("#email").val();
            let phone = $("#phone").val();
            let _token = $("input[name= _token]").val();

            $.ajax({
               url :"{{route('save.student')}}",
                type :"POST",
                data:{
                    first_name:first_name,
                    last_name:last_name,
                    email:email,
                    phone:phone,
                    _token:_token,
                },
                success:function (response) {
                    if(response){
                        $("#studentList tbody").prepend('<tr><td>'+response.first_name+'</td><td>'+response.last_name+'</td><td>'+response.email+'</td><td>'+response.phone+'</td></tr>');
                        $("#add_new_student")[0].reset();
                        $("#addStudent").modal('hide');
                        toastr.success(response.message);
                        table.reload();
                    }
                }
            });
        });
    </script>

    <script>
        function editStudent(id) {
            $.get('student/'+id,function(student) {
                $("#id").val(student.id);
                $("#firstName").val(student.first_name);
                $("#lastName").val(student.last_name);
                $("#e-mail").val(student.email);
                $("#phone_no").val(student.phone);
                $("#updateStudent").modal('toggle');
            });
        }

        $("#update_student").submit(function(e){
            e.preventDefault();
            let id = $("#id").val();
            let first_name = $("#firstName").val();
            let last_name = $("#lastName").val();
            let email = $("#e-mail").val();
            let phone = $("#phone_no").val();
            let _token = $("input[name= _token]").val();
            $.ajax({
                url :"{{route('update.student')}}",
                type:"PUT",
                data:{
                    id:id,
                    first_name:first_name,
                    last_name:last_name,
                    email:email,
                    phone:phone,
                    _token:_token,
                 },
                success:function (response) {
                    $('#sid'+response.id +'td:nth-child(1)').text(response.first_name);
                    $('#sid'+ response.id +'td:nth-child(2)').text(response.last_name);
                    $('#sid'+ response.id +'td:nth-child(3)').text(response.email);
                    $('#sid'+ response.id +'td:nth-child(4)').text(response.phone);
                    $("#updateStudent").modal('toggle');
                    $("#update_student")[0].reset();
                    toastr.success(response.message);
                    $('.dataTable').DataTable().reload();
                }
            });
        });
    </script>
    <script>
        function deleteStudent(id){
            if(confirm('Do you realy want to delete this record?')){
                $.ajax({
                    url:'delete_student/'+id,
                    type:'get',
                    data:{
                        _token:$("input[name=_token]").val()
                    },
                    success:function(response){
                        $("#sid"+id).remove();
                        toastr.success(response.message);
                        table.reload();
                    }
                });
            }
        }
    </script>
    <script>
        $(function (e){
           $("#checkAll").click(function (){
               $(".checkBoxClass").prop('checked',$(this).prop('checked'));
           });

           $("#deleteSelectedRecord").click(function (e){
               e.preventDefault();

               var allIds = [];

               $("input:checkbox[name=ids]:checked").each(function (){
                   allIds.push($(this).val());
               });

               $.ajax({
                  url:"{{route('student.deleteSelected')}}",
                  type:"DELETE",
                   data:{
                      _token:$("input[name=_token]").val(),
                       ids:allIds
                   },
                   success:function (response){
                      $.each(allIds,function(key,val){
                          $("#sid"+val).remove();

                          window.location.reload();

                      })
                   }
               });
           })

        });
    </script>
    <script>
        @if(Session::has('status'))
        toastr.success("{{ Session::get('status') }}");
        @endif
        @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
        @endif
    </script>

    @if(Session::has('delete-success'))
        <script>
            swal("Great Job!","{!! Session::get('delete-success') !!}","success",{
                button:"OK",
            })
        </script>
    @endif

@endsection
