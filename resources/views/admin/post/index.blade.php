<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12" id="post-data">
                    @include('admin.post.data')
                </div>


            </div>
        </div>

    </section>
    <div class="ajax-load text-center" style="display: none;">
        <p><img src="{{asset('images/loader.gif')}}" alt="">Loading..</p>
    </div>

    <footer>
        <script>
            var ENDPOINT = "{{ url('/') }}";
            function loadMoreData(page){
                $.ajax({
                    url: ENDPOINT + '/admin/posts?page=' + page,
                    type:'get',
                    beforeSend:function (){
                        $(".ajax-load").show();
                    }
                })
                .done(function (data){
                    if (data.length == 0) {
                        $('.ajax-load').html("We don't have more data to display :(");
                        return;
                    }
                    $('.ajax-load').hide();
                    $('#post-data').append(data.html);
                })
                .fail(function (jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
            }

            var page = 1;
            //loadMoreData(page);
            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                    page++;
                    loadMoreData(page);
                }
            });

        </script>
    </footer>
</body>
</html>
