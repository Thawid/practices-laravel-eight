@foreach($posts as $post)
<div class="card-header">
    <h4><a href="javascript:void (0);">{{$post->title}}</a></h4>
</div>
<div class="card-body">
    <p>{{$post->body}}</p>
    <div class="text-center">
        <button type="button" class="btn btn-primary">Read More</button>
    </div>
</div>

@endforeach
